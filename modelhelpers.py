

class Validation(object):
    
    __missingfield__ = "Missing field %(key)s"
    
    def _required(self, key, value):
        mf = self.__missingfield__ % {'key':key}
        assert value is not None, mf
        if (isinstance(value, str) or isinstance(value, unicode)):
            assert len(value) > 0, mf
        return value
    
    



